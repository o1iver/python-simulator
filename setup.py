# -*- coding: utf-8 -*-
"""
    setup
    ~~~~~

    :copyright: (c) 2012 by Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
"""

from setuptools import setup

setup(name='pymdp'
     ,version='0.1'
     ,description='Markov Decision Processes library'
     ,url='http://bitbucket.org/o1iver/pymdp'
     ,author='Oliver Stollmann'
     ,author_email='oliver@stollmann.net'
     ,license='BSD3'
     ,packages=['']
     ,test_suite = 'nose.collector'
     ,tests_require=['nose']
     ,zip_safe=True)

