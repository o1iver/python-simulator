import pysim


sim_sys = pysim.SimulationSystem(
                 model = 'testModel5'
                ,steps_per_sim = 4
                ,state_signal_name  = 'stateSignal'
                ,action_signal_name = 'actionSignal'
                ,reward_signal_name = 'rewardSignal'
                ,sim_dir            = '/mnt/hgfs/vm_share_1/sims/'
                ,states_dir         = 'D:\Data\Shares\\vm_share_1\sims\\'

                )


