# -*- coding: utf-8 -*-
"""
    pysim
    ~~~~~

    :copyright: (c) 2012 by Oliver Stollmann
    :license: BSD3, see LICENSE for more details.
"""

import os
import json
import time
import datetime

class Signal(object):
    def __init__(self,name,times=None,values=None):
        self.name   = name
        self.times  = times
        self.values = values

        if (times is not None) and (values is not None):
            assert(len(times) == len(values))
    def to_dict(self):
        return {'name'  : self.name
               ,'times' : self.times
               ,'values': self.values}

    @staticmethod
    def from_dict(obj):
        return Signal(**obj)

class Simulation(object):
    def __init__(self,model,start_time,end_time,inputs,outputs
                ,start_state=None,end_state=None):
        self.model       = model
        self.start_time  = start_time
        self.end_time    = end_time
        self.inputs      = inputs
        self.outputs     = outputs
        self.start_state = start_state
        self.end_state   = end_state

    def to_json(self):
        obj = {'simulation':None}
        obj['simulation'] = {
                'model'         : self.model
                ,'startTime'    : self.start_time
                ,'stopTime'      : self.end_time
                ,'inputs'       : map(lambda s: s.to_dict(),self.inputs )
                ,'outputs'      : map(lambda s: s.to_dict(),self.outputs)
                ,'loadState'    : bool(self.start_state)
                ,'loadStateFile': self.start_state
                ,'saveState'    : bool(self.end_state)
                ,'saveStateFile': self.end_state }
        return json.dumps(obj,indent=2)
    
    @staticmethod
    def from_json(json_):
        obj = json.loads(json_)
        obj = obj['simulation']
        if type(obj['inputs']) is not list:
            obj['inputs'] = [obj['inputs']]
        if type(obj['outputs']) is not list:
            obj['outputs'] = [obj['outputs']]
        return Simulation(model      = obj['model'    ]
                         ,start_time = obj['startTime']
                         ,end_time   = obj['stopTime'  ]
                         ,inputs     = map(lambda s: Signal.from_dict(s)
                                           ,obj['inputs'])
                         ,outputs    = map(lambda s: Signal.from_dict(s)
                                           ,obj['outputs'])
                         ,start_state= obj['loadStateFile']
                         ,end_state  = obj['saveStateFile'])


class SimulationSystem(object):
    def __init__(self,host_os,model,steps_per_sim,sim_wait_time,state_signal_name,action_signal_name
                ,reward_signal_name,sim_dir,states_dir=None
                ,start_state_path=None):

        self.host_os            = host_os # Can be either "win" or "unix"
        self.model              = model
        self.steps_per_sim      = steps_per_sim
        self.sim_wait_time      = sim_wait_time
        self.state_signal_name  = state_signal_name
        self.action_signal_name = action_signal_name
        self.reward_signal_name = reward_signal_name
        self.sim_dir            = sim_dir

        self.sim_in_dir  = os.path.join(sim_dir,'incoming')
        self.sim_out_dir = os.path.join(sim_dir,'outgoing')

        self.sim_in_file  = os.path.join(self.sim_in_dir ,'sim_{}.json').format
        self.sim_out_file = os.path.join(self.sim_out_dir,'sim_{}.json').format


        self.states_dir         = states_dir
        self.start_state_path   = start_state_path

        if self.host_os == "unix":
            self.state_path = os.path.join(states_dir,'{}').format
        else:
            def make_win_path(fname):
                return states_dir + '\\' + fname
            self.state_path = make_win_path

        self.state_name = 'state_model-{}_n-{}_start-{}_end-{}.mat'.format

        self._time = 0
        self._n    = 0

        self._last_state_path = self.start_state_path

        self._state  = 0
        self._reward = 0.0

    def step(self,action,timeout=None):
        timeout = timeout or self.sim_wait_time
        start_time = self._time
        end_time   = self._time + self.steps_per_sim

        print('Start time: %u' % start_time)
        print('End   time: %u' % end_time  )

        input_sig = Signal(name  =self.action_signal_name
                          ,times =range(start_time,end_time+1)
                          ,values=[action]*(self.steps_per_sim+1))
        state_sig = Signal(name   =self.state_signal_name
                           ,times  =None
                           ,values =None)
        reward_sig = Signal(name   =self.reward_signal_name
                           ,times  =None
                           ,values =None)



        if self.states_dir is not None:
                start_state = self._last_state_path
                end_state   = self.state_path(self.state_name(
                    self.model,self._n,start_time,end_time))
                self._last_state_path = end_state
        else:
            start_state = None
            end_state   = None

        print('Start state: %s' % str(start_state))
        print('End   state: %s' % str(end_state  ))

        sim = Simulation(model       =self.model
                        ,start_time  =start_time
                        ,end_time    =end_time
                        ,inputs      =[input_sig]
                        ,outputs     =[state_sig,reward_sig]
                        ,start_state =start_state
                        ,end_state   =end_state)

        sim_json = sim.to_json()

        with open(self.sim_in_file(self._n),'w') as f:
            f.write(sim_json)

        # Get response
        out_file_path = self.sim_out_file(self._n)

        t0 = datetime.datetime.now()
        out_json = None
        print('Trying to get response file (%s) ...' % out_file_path)
        while not out_json:
            time.sleep(0.5)
            print '.',
            try:
                with open(out_file_path) as f:
                    time.sleep(0.5)
                    out_json = f.read()
                    break
            except IOError:
                pass
            
            t1 = datetime.datetime.now()
            if (t1-t0).seconds > timeout:
                raise RuntimeError(
                        'No response file (`%s\') found before timeout (%f). Starttime: %s' %
                            (out_file_path, timeout, str(t0)))

	print ''
        response_sim = Simulation.from_json(out_json)
        outputs = response_sim.outputs
        state  = filter(lambda s: s.name == self.state_signal_name ,outputs)[0]
        reward = filter(lambda s: s.name == self.reward_signal_name,outputs)[0]

        # Get final state value
        self._state  = state.values[-1]
        print('Transitioned to state: %s' % str(self._state))

        # Get sum of reward values
        self._reward = reward.values[-1]
        print('Received reward: %s' % str(self._reward))

        self._n += 1
        self._time = end_time


    def act(self,action,timeout=None):
        self.step(action,timeout)

    def state(self):
        return self._state

    def reward(self):
        return self._reward

