examples Package
================

:mod:`examples` Package
-----------------------

.. automodule:: pymdp.examples
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`blackjack` Module
-----------------------

.. automodule:: pymdp.examples.blackjack
    :members:
    :undoc-members:
    :show-inheritance:

