planning Package
================

:mod:`planning` Package
-----------------------

.. automodule:: pymdp.planning
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`dp` Module
----------------

.. automodule:: pymdp.planning.dp
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`genetic` Module
---------------------

.. automodule:: pymdp.planning.genetic
    :members:
    :undoc-members:
    :show-inheritance:

