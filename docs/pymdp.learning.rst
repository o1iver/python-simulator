learning Package
================

:mod:`learning` Package
-----------------------

.. automodule:: pymdp.learning
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`montecarlo` Module
------------------------

.. automodule:: pymdp.learning.montecarlo
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`q` Module
---------------

.. automodule:: pymdp.learning.q
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`sarsa` Module
-------------------

.. automodule:: pymdp.learning.sarsa
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`td` Module
----------------

.. automodule:: pymdp.learning.td
    :members:
    :undoc-members:
    :show-inheritance:

